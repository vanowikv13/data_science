
from collections import Counter

# folding list
even_numbers = [x for x in range(5) if x % 2 == 0]
squaries = [x * x for x in range(1, 5)]
# print(squaries)
even_squares = [x * x for x in even_numbers]
# print(even_squares)
square_dict = { x : x * x for x in range(1,10)}
square_set = set(x * x for x in [1,-1,2,-2])

increasing_pairs = [(x, y) for x in range(10) for y in [1, 2]]
print(Counter(increasing_pairs))

# generator
def lazy_range(n, start = 2):
    i = start
    while i < n:
        yield i
        i += 2

for x in lazy_range(10):
    print(x)


even_numbers = [x for x in lazy_range(10)]
even_pairs = [(x, y) for x in lazy_range(10) for y in lazy_range(20)]
print(even_pairs)

# Class
class Animal:
    def __init__(self, name, age):
        self.name = name
        self.age = age
    
    def salutation(self):
        print("Hello I'm" + self.name)


a = Animal("Piesel", 3)
a.salutation()

