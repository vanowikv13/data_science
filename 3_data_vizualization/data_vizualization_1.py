from matplotlib import pyplot as plt
from collections import Counter

from data import decile

db = __import__('data')


# wykres liniowy

def line_graph():
    plt.plot(db.years, db.gdp, color='green', marker='o', linestyle='solid')
    plt.title('Nominalny pkg')
    plt.ylabel('Min dol.')


# line_graph()

# wykres słupkowy

def bar_graph():
    xs = [i for i, _ in enumerate(db.movies)]
    plt.bar(xs, db.num_oscars)
    plt.ylabel("Liczba nagrod")
    plt.title("Moje ulubione filmy")


# bar_graph()


def histogram_():
    histogram = Counter(decile(grade) for grade in db.grades)
    plt.bar([x for x in histogram.keys()],
            histogram.values(),
            8)
    plt.axis([-5, 105, 0, 5])
    plt.xticks([10 * i for i in range(11)])
    plt.xlabel("Decyl")
    plt.ylabel("Liczba studentów")


# histogram_()

# grupa wykresów liniowych

def linear_graph():
    total_error = [x + y for x, y in zip(db.variance, db.bias_squared)]
    xs = range(len(db.variance))
    # we can make multiple calls to plt.plot
    # to show multiple series on the same chart
    plt.plot(xs, db.variance, 'g-', label='variance')  # green solid line
    plt.plot(xs, db.bias_squared, 'r-.', label='bias^2')  # red dot-dashed line
    plt.plot(xs, total_error, 'b:', label='total error')  # blue dotted line

    # because we've assigned labels to each series
    # we can get a legend for free
    # loc=9 means "top center"
    plt.legend(loc=9)
    plt.xlabel("model complexity")
    plt.title("The Bias-Variance Tradeoff")


# linear_graph()

def point_graph():
    plt.scatter(db.friends, db.minutes)

    # label each point
    for label, friend_count, minute_count in zip(db.labels, db.friends, db.minutes):
        plt.annotate(label,
                     xy=(friend_count, minute_count),  # put the label with its point
                     xytext=(5, -5),  # but slightly offset
                     textcoords='offset points')

    plt.title("Daily Minutes vs. Number of Friends")
    plt.xlabel("# of friends")
    plt.ylabel("daily minutes spent on the site")


# point_graph()



plt.show()
