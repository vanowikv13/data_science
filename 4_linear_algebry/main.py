# vectors

import math


def vector_add(v, w):
    """sum of vectors"""
    return [v_i + w_i for v_i, w_i in zip(v, w)]


def vector_subtract(v, w):
    """subtract of a vectors"""
    return [v_i - w_i for v_i, w_i in zip(v, w)]


list1 = [1, 2, 3]
list2 = [4, 5, 6]


# print(vector_subtract(list1, list2))


def vector_sum(vectors):
    """sum list of vectors"""
    result = vectors[0]
    for vector in vectors[1:]:
        result = vector_add(result, vector)
    return result


vectors_ = [
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9]
]


# print(vector_sum(vectors_))


def scalar_multiply(c, v):
    return [c * v_ for v_ in v]


# print(scalar_multiply(2, vector_sum(vectors_)))


def vector_mean(vectors):
    """compute the vector whose i-th element is the mean of the
    i-th elements of the input vectors"""
    n = len(vectors)
    vec_sum = vector_sum(vectors)
    return scalar_multiply(1 / n, vec_sum)


# print(vector_mean(vectors_))


def dot(v, w):
    """v_1 * w_1 + ... + v_n * w_n"""
    return sum(v_i * w_i for v_i, w_i in zip(v, w))


def sum_of_squares(v):
    """v_1 * v_1 + ... + v_n * v_n"""
    return dot(v, v)


def magnitude(v):
    return math.sqrt(sum_of_squares(v))


def squared_distance(v, w):
    return sum_of_squares(vector_subtract(v, w))


def distance(v, w):
    return math.sqrt(squared_distance(v, w))


# print(distance([1, 2], [3, 4]))


# Macierze

#          user 0  1  2  3  4  5  6  7  8  9
#
friendships = [[0, 1, 1, 0, 0, 0, 0, 0, 0, 0],  # user 0
               [1, 0, 1, 1, 0, 0, 0, 0, 0, 0],  # user 1
               [1, 1, 0, 1, 0, 0, 0, 0, 0, 0],  # user 2
               [0, 1, 1, 0, 1, 0, 0, 0, 0, 0],  # user 3
               [0, 0, 0, 1, 0, 1, 0, 0, 0, 0],  # user 4
               [0, 0, 0, 0, 1, 0, 1, 1, 0, 0],  # user 5
               [0, 0, 0, 0, 0, 1, 0, 0, 1, 0],  # user 6
               [0, 0, 0, 0, 0, 1, 0, 0, 1, 0],  # user 7
               [0, 0, 0, 0, 0, 0, 1, 1, 0, 1],  # user 8
               [0, 0, 0, 0, 0, 0, 0, 0, 1, 0]]  # user 9


#####
# DELETE DOWN
#

def shape(A):
    num_rows = len(A)
    num_cols = len(A[0]) if A else 0
    return num_rows, num_cols


# print(shape([[1, 2], [3, 4]]))

def get_row(A, i):
    return A[i]


def get_column(A, j):
    return [A_i[j] for A_i in A]

# print((get_column(friendships, 1, 2)))


def make_matrix(num_rows, num_cols, entry_fn):
    """returns a num_rows x num_cols matrix
    whose (i,j)-th entry is entry_fn(i, j)"""
    return [[entry_fn(i, j) for j in range(num_cols)]
            for i in range(num_rows)]


def is_diagonal(i, j):
    """1's on the 'diagonal', 0's everywhere else"""
    return 1 if i == j else 0


identity_matrix = make_matrix(5, 5, is_diagonal)
# print(identity_matrix)

