from collections import Counter, defaultdict
db = __import__('database_set')


users = db.users
friendShips = db.friendShips
interests = db.interests
salaries_and_tenures = db.salaries_and_tenures

for user in users:
    user["friends"] = []

for i, j in friendShips:
    users[i]["friends"].append(users[j])
    users[j]["friends"].append(users[i])

def number_of_friends(user):
    return len(user["friends"])

total_connections = sum(number_of_friends(user) for user in users)

num_users = len(users)
avg_connections = total_connections / num_users

num_friends_by_id = [(user["id"], number_of_friends(user)) for user in users]

# sort by number of friends descending
num_friends_by_id.sort(key=lambda tup: tup[1], reverse=True)


def not_the_same(user, other_user):
    return user['id'] != other_user['id']

def not_friends(user, other_user):
    return all(not_the_same(friend, other_user) for friend in user['friends'])

def friends_of_friends_ids(user):
    return Counter(foaf['id'] 
                   for friend in user['friends'] 
                   for foaf in friend['friends'] 
                   if not_the_same(user, foaf) 
                   and not_friends(user, foaf))

user_ids_by_interest = defaultdict(list)

for user_id, interest in interests:
    user_ids_by_interest[interest].append(user_id)

interests_by_user_id = defaultdict(list)

for user_id, interest in interests:
    interests_by_user_id[user_id].append(interest)

# print(interests_by_user_id)

def most_common_interests_between_two_users(user_id, friend_id):
    counter = 0
    for our_user in interests_by_user_id[user_id]:
        for possibliy_friend in interests_by_user_id[friend_id]:
            print(possibliy_friend)
            if our_user == possibliy_friend:
                counter += 1
    return counter


salary_by_tenure = defaultdict(list)

for salary, tenure in salaries_and_tenures:
    salary_by_tenure[tenure].append(salary)

average_salary_by_tenure = {
    tenure : sum(salaries) / len(salaries)
    for tenure, salaries in salary_by_tenure.items()
}

def tenure_bucket(tenure):
    if tenure < 2:
        return "less than 2"
    elif tenure < 5:
        return "less than 5 more than 2"
    else:
        return "above 5"


salary_by_tenure_bucket = defaultdict(list)

for salary, tenure in salaries_and_tenures:
    bucket = tenure_bucket(tenure)
    salary_by_tenure_bucket[bucket].append(salary)

arr = {
    'x': [1, 2 ,3],
    'y': [2, 4 ,6],
    'z': [3, 6, 9]
    }

average_salary_by_bucket = {
  tenure_bucket : sum(salaries) / len(salaries)
  for tenure_bucket, salaries in salary_by_tenure_bucket.items()
}

average_arr = {
    item: nums[2] - nums[1]
    for item, nums in arr.items()
    }

words_and_counts = Counter(word for user, interest in interests
                           for word in interest.lower().split())

for word, count in words_and_counts.most_common():
    if count > 1:
        print(word, count)