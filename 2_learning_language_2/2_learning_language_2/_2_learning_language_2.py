
# currying functions

def exp(base, power):
    return base ** power

def two_to_the(power):
    return exp(2, power)

from functools import partial
two_to_the = partial(exp, 2)
print(two_to_the(3))

square_of = partial(exp, power=2)
print(square_of(3))

def double(x):
    return 2 * x

xs = [1, 2, 3, 4]
twice_xs = [double(x) for x in xs]
twice_xs = map(double, xs)
list_doubler = partial(map, double)
twice_xs = list_doubler(xs)

def multiply(x, y): return x*y

products = map(multiply, [1,2], [4,5])

# enumerate

documents = ['a', 'b', 'c', 'd']

def writeOut(attribute, index):
    print(str(index) + " " + attribute)

# not good
for i in range(len(documents)):
    document = documents[i]
    writeOut(document, i)

# not good
i = 0
for document in documents:
    writeOut(document, i)
    i+=1

# good
for i, document in enumerate(documents):
    writeOut(document, i)

